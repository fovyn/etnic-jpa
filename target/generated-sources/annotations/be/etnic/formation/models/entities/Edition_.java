package be.etnic.formation.models.entities;

import be.etnic.formation.models.entities.Edition.EditionPK;
import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import java.time.LocalDate;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Edition.class)
public abstract class Edition_ {

	public static volatile SingularAttribute<Edition, Integer> qtt;
	public static volatile SingularAttribute<Edition, LocalDate> outDate;
	public static volatile SingularAttribute<Edition, Book> book;
	public static volatile SingularAttribute<Edition, EditionPK> id;

	public static final String QTT = "qtt";
	public static final String OUT_DATE = "outDate";
	public static final String BOOK = "book";
	public static final String ID = "id";

}

