package be.etnic.formation.models.entities;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Author.class)
public abstract class Author_ {

	public static volatile SingularAttribute<Author, String> firstname;
	public static volatile SingularAttribute<Author, Long> id;
	public static volatile SingularAttribute<Author, String> pseudo;
	public static volatile SingularAttribute<Author, String> lastname;

	public static final String FIRSTNAME = "firstname";
	public static final String ID = "id";
	public static final String PSEUDO = "pseudo";
	public static final String LASTNAME = "lastname";

}

