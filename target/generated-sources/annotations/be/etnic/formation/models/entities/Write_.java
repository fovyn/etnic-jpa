package be.etnic.formation.models.entities;

import be.etnic.formation.models.entities.Write.WritePK;
import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Write.class)
public abstract class Write_ {

	public static volatile SingularAttribute<Write, Float> prc;
	public static volatile SingularAttribute<Write, Author> author;
	public static volatile SingularAttribute<Write, Book> book;
	public static volatile SingularAttribute<Write, WritePK> id;

	public static final String PRC = "prc";
	public static final String AUTHOR = "author";
	public static final String BOOK = "book";
	public static final String ID = "id";

}

