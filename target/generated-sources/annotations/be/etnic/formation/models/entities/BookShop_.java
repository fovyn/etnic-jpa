package be.etnic.formation.models.entities;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BookShop.class)
public abstract class BookShop_ {

	public static volatile SingularAttribute<BookShop, Address> address;
	public static volatile SingularAttribute<BookShop, String> name;
	public static volatile SingularAttribute<BookShop, Long> id;

	public static final String ADDRESS = "address";
	public static final String NAME = "name";
	public static final String ID = "id";

}

