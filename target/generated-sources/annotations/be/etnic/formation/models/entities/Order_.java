package be.etnic.formation.models.entities;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import java.time.LocalDate;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Order.class)
public abstract class Order_ {

	public static volatile SingularAttribute<Order, Integer> qtt;
	public static volatile SingularAttribute<Order, BookShop> bookShop;
	public static volatile SingularAttribute<Order, Edition> edition;
	public static volatile SingularAttribute<Order, Long> id;
	public static volatile SingularAttribute<Order, LocalDate> orderDate;

	public static final String QTT = "qtt";
	public static final String BOOK_SHOP = "bookShop";
	public static final String EDITION = "edition";
	public static final String ID = "id";
	public static final String ORDER_DATE = "orderDate";

}

