package be.etnic.formation.stores.book;

import be.etnic.formation.libs.store.Action;
import be.etnic.formation.libs.store.Reducer;
import be.etnic.formation.models.entities.Book;
import be.etnic.formation.stores.book.actions.AddBookAction;
import jakarta.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;

public class BookReducer implements Reducer<BookState> {
    private final EntityManager em;

    public BookReducer(EntityManager em) {
        this.em = em;
    }

    @Override
    public BookState reduce(BookState state, Action action) {
        if (action instanceof AddBookAction) {
            return this.addBookAction(state, (Book) action.getPayload());
        }

        throw new RuntimeException();
    }

    private BookState addBookAction(BookState state, Book book) {
        List<Book> oldState = state.getBooks();

        em.getTransaction().begin();
        em.persist(book);
        em.getTransaction().commit();

        List<Book> newState = new ArrayList<>(oldState);
        newState.add(book);
        return new BookState(newState);
    }
}
