package be.etnic.formation.stores.book;

import be.etnic.formation.libs.Console;
import be.etnic.formation.libs.store.Action;
import be.etnic.formation.models.entities.Book;
import be.etnic.formation.stores.book.actions.AddBookAction;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class BookActions {

    public static Action addBook() {
        Book book = new Book();
        log.info("Encoder l'isbn: ");
        book.setIsbn(Console.readLine());
        log.info("Encoder le titre: ");
        book.setTitle(Console.readLine());

        return new AddBookAction(book);
    }
}
