package be.etnic.formation.stores.book.actions;

import be.etnic.formation.libs.store.Action;
import be.etnic.formation.models.entities.Book;

public class AddBookAction implements Action {
    private final Book book;

    public AddBookAction(Book book) {
        this.book = book;
    }

    @Override
    public Object getPayload() {
        return book;
    }
}
