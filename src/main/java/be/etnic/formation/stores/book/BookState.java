package be.etnic.formation.stores.book;

import be.etnic.formation.models.entities.Book;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BookState {
    private final List<Book> books;

    public BookState(Collection<Book> books) {
        this.books = new ArrayList<>(books);
    }

    public List<Book> getBooks() {
        return new ArrayList<>(books);
    }
}
