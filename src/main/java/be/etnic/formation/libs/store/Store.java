package be.etnic.formation.libs.store;

import java.util.ArrayList;
import java.util.List;

public class Store<T> {
    private final List<Subscriber<T>> subscribers = new ArrayList<>();
    public Reducer<T> reducer;
    private T currentState;


    private Store(T initialState, Reducer<T> reducer) {
        this.currentState = initialState;
        this.reducer = reducer;
    }

    public static <T> Store<T> createStore(T initialState, Reducer<T> reducer) {
        return new Store<>(initialState, reducer);
    }

    public T dispatch(Action action) {
        T newState = this.reducer.reduce(this.currentState, action);

        this.subscribers.forEach(s -> s.execute(newState));
        this.currentState = newState;

        return newState;
    }

    public Subscription subscribe(Subscriber<T> subscriber) {
        this.subscribers.add(subscriber);
        subscriber.execute(this.currentState);

        return () -> subscribers.remove(subscriber);
    }

    public T getState() {
        return this.currentState;
    }
}
