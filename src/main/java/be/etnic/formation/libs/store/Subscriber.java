package be.etnic.formation.libs.store;

@FunctionalInterface
public interface Subscriber<T> {
    void execute(T state);
}
