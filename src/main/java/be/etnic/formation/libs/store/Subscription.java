package be.etnic.formation.libs.store;

@FunctionalInterface
public interface Subscription {
    void unsubscribe();
}
