package be.etnic.formation.libs.store;

public interface Action {
    Object getPayload();
}
