package be.etnic.formation.libs.store;

public interface Reducer<T> {
    T reduce(T state, Action action);
}
