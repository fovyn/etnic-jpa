package be.etnic.formation.libs;

import java.util.Scanner;

public final class Console {
    private static Scanner getScanner() {
        return new Scanner(System.in);
    }

    public static String readLine() {
        return getScanner().nextLine();
    }
}
