package be.etnic.formation;

import be.etnic.formation.libs.store.Store;
import be.etnic.formation.models.entities.Book;
import be.etnic.formation.stores.BookReducer;
import be.etnic.formation.stores.book.BookState;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

import static be.etnic.formation.libs.store.Store.createStore;
import static be.etnic.formation.stores.book.BookActions.addBook;

@Slf4j
public class Main {

    public static void main(String[] args) {
        log.info("ETNIC-JPA: started");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("etnicjpa");
        EntityManager em = emf.createEntityManager();

        Store<BookState> bookStore = createStore(new BookState(new ArrayList<>()), new BookReducer(em));
        bookStore.subscribe((state) -> {
            state.getBooks().forEach(it -> log.info("{}", it));
        });

        Book book = new Book();
        book.setIsbn("1-bww-114");
        book.setTitle("Je fais du JPA avec Fla");

        bookStore.dispatch(addBook());


        em.close();
        emf.close();
        log.info("ETNIC-JPA: stopped");
    }
}