package be.etnic.formation.models.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Data;

@Embeddable
@Data

public class Address {
    @Column(name = "address_street")
    private String street;
    @Column(name = "address_number")
    private String number;
    @Column(name = "address_city")
    private String city;
    @Column(name = "address_country")
    private String country;
}
