package be.etnic.formation.models.entities;

import jakarta.persistence.Basic;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Entity
@Data
public class Book implements Serializable {
    @Id
    private String isbn;

    @Basic(optional = false)
    private String title;
    @ElementCollection
    private List<String> disctinctions;

}
