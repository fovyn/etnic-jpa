package be.etnic.formation.models.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
public class Edition implements Serializable {
    @EmbeddedId
    private EditionPK id = new EditionPK();

    @Basic(optional = false)
    private int qtt;
    @Basic(optional = false)
    private LocalDate outDate;

    @ManyToOne(targetEntity = Book.class)
    @MapsId(value = "isbn")
    private Book book;

    @Embeddable
    @Data
    public static class EditionPK implements Serializable {
        @Column(name = "book_isbn")
        private String isbn;
        @Column(name = "edition_order")
        private Integer order;
    }
}
