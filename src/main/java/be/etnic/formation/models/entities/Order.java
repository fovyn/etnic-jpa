package be.etnic.formation.models.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "book_order")
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    private LocalDate orderDate;
    @Basic(optional = false)
    private int qtt;

    @ManyToOne(targetEntity = Edition.class, optional = false)
    private Edition edition;
    @ManyToOne(targetEntity = BookShop.class, optional = false)
    private BookShop bookShop;
}
