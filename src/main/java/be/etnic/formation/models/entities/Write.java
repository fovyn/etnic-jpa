package be.etnic.formation.models.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;

@Entity
@Data
public class Write implements Serializable {
    @EmbeddedId
    private WritePK id = new WritePK();

    @Basic(optional = false)
    private float prc;

    @ManyToOne(targetEntity = Book.class)
    @MapsId(value = "isbn")
    private Book book;

    @ManyToOne(targetEntity = Author.class)
    @MapsId(value = "authorId")
    private Author author;

    @Embeddable
    @Data
    public static class WritePK implements Serializable {
        @Column(name = "book_isbn")
        private String isbn;
        @Column(name = "author_id")
        private Long authorId;
    }
}
