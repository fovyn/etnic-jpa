package be.etnic.formation.models.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;

@Entity
@Data
public class Author implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    private String lastname;
    @Basic(optional = false)
    private String firstname;
    @Basic(optional = false)
    private String pseudo;
}
